export PYTHONPATH="${PYTHONPATH}:${HOME}/pyhton/"


# ----------
# Virtualenv
# ----------
export WORKON_HOME="${HOME}/Envs"
export PROJECT_HOME="${HOME}/pyprojects"
export VIRTUALENVWRAPPER_VIRTUALENV_ARGS='--no-site-packages'
export PIP_VIRTUALENV_BASE="${WORKON_HOME}"
export PIP_RESPECT_VIRTUALENV=truecompinit
export VIRTUALENVWRAPPER_SCRIPT="/usr/local/bin/virtualenvwrapper.sh"
export VIRTUALENVWRAPPER_HOOK_DIR="${HOME}/Envs"
