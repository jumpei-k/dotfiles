#!/bin/sh
#
# Homebrew
#
# This installs some of the common dependencies needed (or at least desired)
# using Homebrew.

# Check for Homebrew
if test ! $(which brew)
then
  echo "  Installing Homebrew for you."
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  # Install homebrew packages
  #brew install grc coreutils spark

  # Install all packages from brewlist file
  # brewlist="$(pwd)/brewlists.txt"
  #
  # if [ -f $brewlist]
  #   then
  #     echo " Installing all your brew packages..."
  #     value=$(<$brewlist)
  #     echo "Following packages will be installed.."
  #     echo "$value"
  #     `"${value}" | xargs brew install`
  #   else
  #     echo "Invalid file name"
  # fi
  # Keep updating brewlist.txt
  # You can use `cat list.txt | xargs brew install` on Terminal instead.

  # exit 0
fi
