# init according to man page
# 1. Sets up your shims path. This is the only requirement for rbenv to function properly. You can do this by hand by prepending ~/.rbenv/shims to your $PATH.
# 2. Rehashes shims — `rbenv rehash`

if (( $+commands[rbenv] ))
then
  eval "$(rbenv init -)"
  echo "config success"
fi
