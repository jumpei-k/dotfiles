# Make a directory and move there.
function take() {
  mkdir -p $1
  cd $1
}

# Print Top command rainking
function zsh_stats() {
  fc -l 1 | awk '{CMD[$2]++;count++;}END { for (a in CMD)print CMD[a] " " CMD[a]/count*100 "% " a;}' | grep -v "./" | column -c3 -s " " -t | sort -nr | nl |  head -n20
}

# Convert an animated video to gif
# Works best for videos with low color palettes like Dribbble shots
#
# @param $1 - video file name like `animation.mov`
# @param @optional $2 - resize parameter as widthxheight like `400x300`
#
# Example: vidtogif animation.mov 400x300
# Requirements: ffmpeg and gifsicle. Can be downloaded via homebrew
#
# http://chrismessina.me/b/13913393/mov-to-gif

function vidtogif() {
    if [ -n "$1" ]
        then
            mkdir pngs gifs
            ffmpeg -i "$1" -r 10 pngs/frame_%04d.png
            sips -s format gif pngs/*.png --out gifs/
            cd gifs
            if [ -z "$2" ]
                then
                    gifsicle *.gif --optimize=3 --delay=3 --loopcount > ../animation.gif
                else
                    gifsicle *.gif --optimize=3 --delay=3 --loopcount --resize "$2" > ../animation.gif
            fi
            cd ..
            rm -rf pngs gifs
    else
        echo "Use video file as first parameter"
    fi
}

# get public ipaddress quickly
function myip() {
    curl ipecho.net/plain ; echo
    echo `ipconfig getifaddr en0`
}


# A script to make using 256 colors in zsh less painful.
# P.C. Shyamshankar <sykora@lucentbeing.com>
# Copied from http://github.com/sykora/etc/blob/master/zsh/functions/spectrum/

# BG: Background
# FG: Foreground
# Ag:
typeset -Ag FX FG BG
# Font Type
FX=(
    reset     "%{[00m%}"
    bold      "%{[01m%}" no-bold      "%{[22m%}"
    italic    "%{[03m%}" no-italic    "%{[23m%}"
    underline "%{[04m%}" no-underline "%{[24m%}"
    blink     "%{[05m%}" no-blink     "%{[25m%}"
    reverse   "%{[07m%}" no-reverse   "%{[27m%}"
)

# Assign 256 color to the arrays
for color in {000..255}; do
    FG[$color]="%{[38;5;${color}m%}"
    BG[$color]="%{[48;5;${color}m%}"
done


ZSH_SPECTRUM_TEXT="${ZSH_SPECTRUM_TEXT}:We are all colors. There are 256 colors"

# Show all 256 colors with color number
function spectrum_ls() {
  for code in {000..255}; do
    print -P -- "$code: %F{$code}$ZSH_SPECTRUM_TEXT%f"
  done
}

# Show all 256 colors where the background is set to specific color
function spectrum_bls() {
  for code in {000..255}; do
    print -P -- "$BG[$code]$code: $ZSH_SPECTRUM_TEXT %{$reset_color%}"
  done
}
