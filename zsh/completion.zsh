unsetopt menu_complete   # do not autoselect the first completion entry
unsetopt flowcontrol
setopt autocd		         # move direcotry without cd command
setopt auto_menu         # show completion menu on succesive tab press
setopt complete_in_word

setopt hash_list_all			# hash everything before completion
setopt completealiases			# complete aliases
setopt always_to_end			# when completing from the middleof a word, move the cursor to the end of the word

setopt list_ambiguous			# complete as much of a completion until it gets ambiguous.


zstyle ':completion::complete:*' use-cache on               # completion caching, use rehash to clear
zstyle ':completion:*' cache-path ~/.zsh/cache              # cache path

zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'   # ignore case
# matches case insensitive for lowercase
# zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

zstyle ':completion:*' menu select= 2                      # menu if nb items > 5
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}       # colorz !
zstyle ':completion:*::::' completer _expand _complete _ignored _approximate # list of completers to use

# sections completion !
zstyle ':completion:*' verbose yes
zstyle ':completion:*:descriptions' format $'\e[00;34m%d'
zstyle ':completion:*:messages' format $'\e[00;31m%d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*:manuals' separate-sections true
zstyle ':completion:*:processes' command 'ps -au$USER'
zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:kill:*' force-list always
zstyle ':completion:*:*:kill:*:processes' list-colors "=(#b) #([0-9]#)*=29=34"
zstyle ':completion:*:*:killall:*' menu yes select
zstyle ':completion:*:killall:*' force-list always
users=(jvoisin root)           # because I don't care about others
zstyle ':completion:*' users $users

# pasting with tabs doesn't perform completion
zstyle ':completion:*' insert-tab pending
