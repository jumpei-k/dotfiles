#add each topic folder to fpath so that they can add functions and completion scripts
# Topic folder is a folder on dotfile/
for topic_folder ($ZSH/*) if [ -d $topic_folder ]; then  fpath=($topic_folder $fpath); fi;
