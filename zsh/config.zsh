if [[ -n $SSH_CONNECTION ]]; then
  export PS1='%m:%3~$(git_info_for_prompt)%# '
else
  export PS1='%3~$(git_info_for_prompt)%# '
fi


# What colors use for what type of files/folders
# Useful site http://geoff.greer.fm/lscolors/
export LSCOLORS="exfxcxdxbxegedabagacad"
# export LSCOLORS="exfxcxdxbxegedabagacad"

fpath=($ZSH/functions $fpath)

autoload -U $ZSH/functions/*(:t)

#---------
# History
#---------

HIST_STAMPS="mm/dd/yyyy"

# Show history
if [ "$HIST_STAMPS" = "mm/dd/yyyy" ]
then
    alias history='fc -fl 1'
elif [ "$HIST_STAMPS" = "dd.mm.yyyy" ]
then
    alias history='fc -El 1'
elif [ "$HIST_STAMPS" = "yyyy-mm-dd" ]
then
    alias history='fc -il 1'
else
    alias history='fc -l 1'
fi

HISTFILE=~/.zsh_history         # where to store zsh config
HISTSIZE=5000
SAVEHIST=5000
setopt append_history           # append
setopt HIST_IGNORE_ALL_DUPS     # no duplicate
setopt EXTENDED_HISTORY         # add timestamps to history
unsetopt hist_ignore_space      # ignore space prefixed commands
setopt HIST_REDUCE_BLANKS       # trim blanks
setopt HIST_VERIFY              # show before executing history commands
setopt inc_append_history       # add commands as they are typed, don't wait until shell exit
setopt share_history            # share hist between sessions
setopt bang_hist                # !keyword
setopt APPEND_HISTORY           # adds history
setopt INC_APPEND_HISTORY SHARE_HISTORY  # adds history incrementally and share it across sessions


# Make some commands not show up in history
export HISTIGNORE="ls:cd:cd -:pwd:exit:date:* --help";


#--------
# General
#--------

setopt auto_cd                  # if command is a path, cd into it
setopt auto_remove_slash        # self explicit
setopt chase_links              # resolve symlinks
setopt CORRECT                  # try to correct spelling of commands
setopt extended_glob            # activate complex pattern globbing
setopt glob_dots                # include dotfiles in globbing
setopt print_exit_value         # print return value if non-zero
unsetopt beep                   # no bell on error
unsetopt bg_nice                # no lower prio for background jobs
unsetopt clobber                # must use >| to truncate existing files
unsetopt hist_beep              # no bell on error in history
unsetopt hup                    # no hup signal at shell exit
unsetopt IGNORE_EOF             # do not exit on end-of-file
unsetopt list_beep              # no bell on ambiguous completion
unsetopt rm_star_silent         # ask for confirmation Afor `rm *' or `rm path/*'
setopt COMPLETE_IN_WORD
setopt LOCAL_OPTIONS            # allow functions to have local options
setopt NO_BG_NICE               # don't nice background tasks
setopt NO_HUP
setopt NO_LIST_BEEP
setopt LOCAL_TRAPS              # allow functions to have local traps
setopt PROMPT_SUBST


# don't expand aliases _before_ completion has finished
#   like: git comm-[tab]
setopt complete_aliases

zle -N newtab

#---------------------
# Vim like key-binding
#---------------------

#bindkey -d # Reset bindkey
bindkey -v
bindkey "^P" vi-up-line-or-history
bindkey "^N" vi-down-line-or-history
bindkey "^F" forward-word
bindkey "^B" backward-word
bindkey "^A" beginning-of-line
bindkey "^E" end-of-line


# Uncomment the following line to display red dots whilst waiting for completion.
 COMPLETION_WAITING_DOTS="true"
# Figure out the SHORT hostname
if [[ "$OSTYPE" = darwin* ]]; then
  # OS X's $HOST changes with dhcp, etc. Use ComputerName if possible.
  SHORT_HOST=$(scutil --get ComputerName 2>/dev/null) || SHORT_HOST=${HOST/.*/}
else
  SHORT_HOST=${HOST/.*/}
fi
# Save the location of the current completion dump file.
if [ -z "$ZSH_COMPDUMP" ]; then
  ZSH_COMPDUMP="${ZDOTDIR:-${HOME}}/.zcompdump-${SHORT_HOST}-${ZSH_VERSION}"
fi

unset file


## smart urls
autoload -U url-quote-magic
zle -N self-insert url-quote-magic


#------
# Pushd
#------

setopt auto_pushd               # make cd push old dir in dir stack
setopt pushd_ignore_dups        # no duplicates in dir stack
setopt pushd_silent             # no dir stack after pushd or popd
setopt pushd_to_home            # `pushd` = `pushd $HOME`

setopt long_list_jobs
# setxkbmap -option compose:ralt  # compose-key
print -Pn "\e]0; %n@%M: %~\a"   # terminal title

### No quotation between file names
autoload -Uz zmv
alias zmv='noglob zmv -W'

source /usr/local/bin/virtualenvwrapper_lazy.sh
