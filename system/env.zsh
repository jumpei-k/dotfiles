export EDITOR='vim'

#enables colors in the terminal
export CLICOLOR=true

## pager
export PAGER="less"
export LESS="-R"

### Prefer US English and use UTF-8

export LANG="en_US.UTF-8";
export LC_ALL="en_US.UTF-8";

export LC_CTYPE=$LANG

### Highlight section titles in manual pages
export LESS_TERMCAP_md="${yellow}";
### Don’t clear the screen after quitting a manual page
export MANPAGER="less -X";
