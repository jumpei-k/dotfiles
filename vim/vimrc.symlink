syntax on

" Wrap gitcommit file types at the appropriate length
filetype indent plugin on


" GENERAL SETTING
"""""""""""""""""""""


execute pathogen#infect()
execute pathogen#helptags()
" Enable syntax highlighting
syntax on
" Highlight current line
set cursorline


set encoding=utf-8
let g:airline#extensions#tabline#enabled = 1
" Always show status line
set laststatus=2
" Set leader key
let mapleader=","
" set localleader key
let maplocalleader="-"
"" Enable loading the indent file and plugin for specific file
" Use the OS clipboard by default
set clipboard=unnamed
filetype plugin indent on
" Enhance command-line completion
set wildmenu
" Allow backspace in insert mode
set backspace=indent,eol,start
" Optimize for fast terminal connections
set ttyfast
" Show 'Invisible' characters
set lcs=tab:▸\ ,trail:·,eol:¬,nbsp:_
" Shoe the filename in the window titlebar
set title
" Start scrolling three lines before the horizontal window border
set scrolloff=3

""""""""""""""""""""
" PLUGIN
""""""""""""""""""""
" Setting NERDTree
" Automatically execute NEDTREE
"autocmd vimenter * NERDTree

" ----------------
" Solarlize
" ----------------
"
syntax enable
"let g:solarlize_termtrans = 0
set background=dark
"colorscheme solarized
"togglebg#map("<F5>")
"togglebg#map("<F5>")
"
" ----------------------------
" Key Mapping
" ----------------------------
" Use jk instead of <Esc>
inoremap jk <Esc>
" Disable the old keys
inoremap <Esc> <nop>

"
"  ---------------------------
"  NORMAL
"  ---------------------------
"
"  Yank text to OSX clipbord
noremap <leader>y "*y
noremap <leader>yy "*Y
"
"  Preserve Indentation while pasting text from clipbord
noremap <leader>p :set paste<CR>:put *<CR>:set nopaste<CR>
"  Add highlight
nnoremap n nzz
nnoremap N Nzz
nnoremap * *zz
nnoremap # #zz
nnoremap g* g*zz" Easy write
" Easy write
nnoremap :w :w<CR>
" Easy quit
nnoremap :q :q<CR>
" Quick open
" Open vimrc file in vertically splited window
nnoremap <leader>ev :vsplit $VIMRC<CR>
" Source vimrc
nnoremap <leader>sv :source $VIMRC<CR>
nnoremap <leader>ez :vsplit $ZSHRC<CR>
" Stamp current date yyyy-mm-dd
" nnoremap <leader>td :r!date "+\%F"<CR>
nnoremap <leader>td :r!date "+\%F"<CR>
" Select all lines
nnoremap <leader>all ggvG
" Moving around between buffers easily
nnoremap <leader>l :ls<CR>:b<space>
" Highlight search
set hlsearch
set showmatch
set matchtime=4
set matchpairs& matchpairs+=<:>
set backspace=indent,eol,start

set nowritebackup
set nobackup
set noswapfile
set list
" Ensable line numbers
set number
set wrap
set textwidth=0
set colorcolumn=80

set t_vb=
set novisualbell

set listchars=tab:»-,trail:-,extends:»,precedes:«,nbsp:%,eol:↲

ca w!! w !sudo tee "%"

" folding
" Vimscript file settings {{{
augroup filetype_vim
	autocmd!
	autocmd FileType vim setlocal foldmethod=marker
augroup END
" }}}


" ---------
"  Powerline
"  --------
"
set rtp+=/usr/local/lib/python3.4/site-packages/powerline/bindings/vim


"export TERM="xterm-256color"

" --------
"  Use Powerline Symboles
"  ---------
let g:Powerline_symbols = 'fancy'
set guifont=Inconsolata-g\ for\ Powerline
let g:airline_powerline_fonts=1
" Remove default mode indicator
"
"
"
"-------------------------
"python3 interpreter
"-------------------------
let g:syntastic_python_python_exec = "/usr/local/bin/python3"
let g:syntastic_python_flake8_args = '--ignore=E501,E126'
" Correcting typo
iabbrev teh the
iabbrev waht what
